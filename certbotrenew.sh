#!/bin/bash

if [ "$EUID" -ne 0 ] ; then
  echo "Please run as root"
  exit
fi

PATH=$PATH:/usr/sbin

certcommand="certbot"
certargs="renew --standalone --installer apache"

for arg in $*; do
	if [[ $* == *--dry-run* ]]; then
		$args="$args --dry-run"
	fi
done

service apache2 stop

for fullfile in /etc/letsencrypt/renewal/*.conf; do
	filename=$(basename -- "$fullfile")
	filename="${filename%.*}"

	pgrep apache2 | xargs kill -9 2>/dev/null
	echo "Apache Killed"

	$certcommand $certargs --cert-name $filename
done

pgrep apache2 | xargs kill -9 2>/dev/null
echo "Apache Killed"
service apache2 start

