# Certbot Manager

Certbot has problems with not shutting down apache and giving me failed to bind errors, so I made this script to smother apache with a pillow before each certificate attempts to renew

# Usage

Certbot seems to require root so make sure to run this script as root

Make sure you give certbotrenew.sh executable permissions

```bash
sudo chmod +x certbotrenew.sh
```

To run the script

```bash
sudo ./certbotrenew.sh
```

You should probably test this script, you can do this by adding --dry-run to the end when running the script

```bash
sudo ./certbotrenew.sh --dry-run
```
